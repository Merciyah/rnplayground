import React, {useState} from 'react'
import { View, Text, ImageBackground, Image } from 'react-native'
import styled from "styled-components/native";
import shotGun1 from "../assets/capa.png";
import shotGun2 from "../assets/berretta.png";
import shotGun3 from "../assets/phantom.png";
import uncracked from "../assets/david.jpg";


export default function shop() {
    const prices = () => {

    };
    
    return (
      <ImageBackground source={uncracked} resizeMode={"stretch"} style={{flex:1}}>
        <View
          style={{
            width: 200,
            height: 100,
            flexDirection:'row',
            backgroundColor: "rgba(0,0,0,0.4)",
            alignItems: "center",
            justifyContent: "space-around",
          }}
        >
            <Image source={shotGun1} resizeMode={'contain'} style={{width:60, height:60}} />
            <Image source={shotGun2} resizeMode={'contain'} style={{width:60, height:60}} />
            <Image source={shotGun3} resizeMode={'contain'} style={{width:60, height:60}} />
        </View>
        <Text>Hello Shop</Text>
      </ImageBackground>
    )
}
