import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Setup from './canvas/setup'
import Starbucks from './canvas/starbucks'
import TrigCanvas from './canvas/trigCanvas'
import Shop from './canvas/shop'

export default function App() {
  return (
    <View style={styles.container}>
      <TrigCanvas />
      <StatusBar />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
});
